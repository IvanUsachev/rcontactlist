package com.example.maximus.rcontactlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.ArrayList;


public class AddNewContactActivity extends AppCompatActivity {

    private ArrayList<Contact> contacs;
    Button addContact, buttonReturn;
    EditText getName, getAddress, getEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_contact);

        addContact = (Button) findViewById(R.id.addcontact);
        buttonReturn = (Button) findViewById(R.id.breturn);

        getName = (EditText) findViewById(R.id.name);
        getAddress = (EditText) findViewById(R.id.address);
        getEmail = (EditText) findViewById(R.id.email);

        contacs = new ArrayList<Contact>(Generator.generate());

        buttonReturn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddNewContactActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contacs.add(3, new Contact(getName.toString(), getAddress.toString(), getEmail.toString(), R.mipmap.ic_launcher));

            }
        });

    }
}


