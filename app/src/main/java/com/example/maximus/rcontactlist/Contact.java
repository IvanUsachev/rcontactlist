package com.example.maximus.rcontactlist;

public class Contact {
    private String name;
    private String email;
    private String address;
    private int photo;

    public Contact(String name, String email, String address, int photo) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public int getPhoto() {
        return photo;
    }
}
