package com.example.maximus.rcontactlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;



public class MainActivity extends AppCompatActivity implements MyRecyclerViewAdapter.OnRecyclItemClic {

    MyRecyclerViewAdapter adapter;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new MyRecyclerViewAdapter(this, Generator.generate(), this));
        adapter = new MyRecyclerViewAdapter(this, Generator.generate(), this);

        button = (Button) findViewById(R.id.button);
        recyclerView.setAdapter(adapter);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, AddNewContactActivity.class);
//                startActivity(intent);
                adapter.addItem(new Contact("New contact","new@email.com","new address",R.drawable.icon));
                recyclerView.smoothScrollToPosition(0);
            }
        });
    }

    @Override
    public void onClick(int position) {
        Intent intent = new Intent(MainActivity.this, InfoContactActivity.class);
        intent.putExtra("name", adapter.getItem(position).getName());
        intent.putExtra("address", adapter.getItem(position).getAddress());
        intent.putExtra("email", adapter.getItem(position).getEmail());
        startActivity(intent);

    }
}

