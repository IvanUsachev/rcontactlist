package com.example.maximus.rcontactlist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<Contact> contacts;
    private OnRecyclItemClic onRecyclItemClic;

    public MyRecyclerViewAdapter(Context context, List<Contact> contacts, OnRecyclItemClic onRecyclItemClic) {
        this.context = context;
        this.contacts = contacts;
        this.onRecyclItemClic = onRecyclItemClic;
    }

    public interface OnRecyclItemClic {
        void onClick(int position);
    }

    public void addItem(Contact contact) {
        contacts.add(1, contact);
        notifyItemInserted(1);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.r_list_object, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Contact contact = contacts.get(position);
        holder.icon.setImageResource(contact.getPhoto());
        holder.name.setText(contact.getName());
        holder.email.setText(contact.getEmail());
    }

    public void removeItem(int position) {
        if (position >= 0 && position < contacts.size()) {
            contacts.remove(position);
            notifyItemRangeRemoved(position, 1);
        }
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public Contact getItem(int position) {
        return contacts.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView name;
        TextView email;

        public ViewHolder(View item) {
            super(item);
            icon = (ImageView) item.findViewById(R.id.photo);
            name = (TextView) item.findViewById(R.id.name);
            email = (TextView) item.findViewById(R.id.email);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRecyclItemClic.onClick(getAdapterPosition());
                }
            });
        }

    }
}
